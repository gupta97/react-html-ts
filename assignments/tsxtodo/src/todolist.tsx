import React, { Component } from "react";
import "./ToDoApp.css";
import Modal from "./edit";
import CheckIcon from '@material-ui/icons/Check';
import Icon from '@material-ui/icons/DeleteSharp';

let localdata:any = JSON.parse(localStorage.getItem("user1")!)
let updatetext: any;

type MyProps = {};
type MyState = { isOpen: boolean };

class tsxlist extends Component<MyProps, MyState> {
    constructor(props: MyProps | Readonly<MyProps>) {
        super(props);
        this.state = {isOpen:false};
      }
// const ToDo = () =>{
    AddItem = () => {
        var item =(document.getElementById('itemtoadd') as HTMLInputElement).value;
        localdata = JSON.parse(localStorage.getItem("user1")!)
        if (item === ''){
            alert("You must write something!");
        }
        else if (localdata){
            localdata.push(item)
            localStorage.setItem("user1",JSON.stringify(localdata))
        }
        else{
            var newdata = []
            newdata.push(item)
            localStorage.setItem("user1",JSON.stringify(newdata))
        }
        (document.getElementById("itemtoadd") as HTMLInputElement).value = "";
        localdata = JSON.parse(localStorage.getItem("user1")!)
        if (localdata){
            return (JSON.parse(localStorage.getItem("user1")!))
        }
    }
    checked = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>,index: number) =>{
        e.stopPropagation();
        let list = document.querySelector('ul');
        if(list){
            let li = list.getElementsByTagName("li");
            let currentli = li[index];
            if (currentli.className === "checked"){
                currentli.className = "demo";
            }
            else{
                currentli.className = "checked"
            }
        }
    }
    toggleModal = () => {
        this.setState({
          isOpen: !this.state.isOpen
        });
      }
    
    close = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>, index: number) =>{
        e.stopPropagation();
        // this.togglepopup()
        let list = document.querySelector('ul');
        let currentli
        if (list){
            let li = list.getElementsByTagName("li")
            currentli = li[index]
            var localData = JSON.parse(localStorage.getItem("user1")!);
            localData.splice(index, 1)
            localStorage.removeItem("user1")
            localStorage.setItem("user1", JSON.stringify(localData))
            currentli.className = "hide";
        }
        return currentli
    }
    edit = (index: number, data: number) =>{
        // localStorage.setItem("index",{index})
        this.toggleModal()
        updatetext = JSON.parse(localStorage.getItem("user1")!)[index]
        localStorage.setItem("index", JSON.stringify(index))
    }
    save = () =>{
        var data: any = (document.getElementById("update") as HTMLInputElement).value;
        var localarray: any = JSON.parse(localStorage.getItem("user1")!)
        var oldtext: number = JSON.parse(localStorage.getItem("index")!)
        localarray[oldtext] = data
        localStorage.setItem("user1", JSON.stringify(localarray))
        localStorage.removeItem("index")
        window.location.reload();
    }
    discard = () =>{
        window.location.reload();
    }
    render(){
        return(
            <div className="back">
                <Modal show={this.state.isOpen}>
                    <form>
                        <textarea className="editinput" id="update" defaultValue={[updatetext]}/>
                    </form>
                    <CheckIcon className="buttonsave" onClick={this.save}></CheckIcon>
                    <Icon className="buttondiscard" onClick={this.discard}></Icon>
                </Modal>
                <p>ToDo List</p>
                <div className="aligndiv">  
                    <form>
                        <input className="inputapp" type="text" id="itemtoadd" placeholder="Add a Task"/>
                        <button className="buttonadd" onClick={this.AddItem}>Add Task</button>
                    </form>
                </div>    
                <div>
                    <ul id="myUL">
                        {localdata.map((data: number, index: number) => 
                            <li onClick={() => {this.edit(index,data)}}>{data}
                            <span onClick={(e) => {this.checked(e,index)}}><input type="checkbox" className="markdone"/></span>
                            <span className="close" onClick={(e) => {this.close(e,index)}}>&times;</span>
                            </li>)
                        }
                    </ul>
                </div>
            </div>
        );
    }
}
export default tsxlist;