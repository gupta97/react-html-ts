/* eslint-disable react/jsx-pascal-case */
import React from 'react';
import Login from './login';
import Signup from './signup';
import {BrowserRouter as Router,Switch,Route} from "react-router-dom";
import {Todo} from './redux/todoComponent'
import Profile from './profile';


const Main= () => {

    return(<div>
        <Router>
         <Switch>
        <Route exact path="/">
            <Login/>
        </Route>
        <Route path="/signup">
            <Signup />,
        </Route>
        <Route path="/todo" >
            <Todo state={[]} />
            </Route>
        <Route path="/profile">
            < Profile/>,
        </Route>
    </Switch>
    </Router>
   
    </div>)
}
export default Main;