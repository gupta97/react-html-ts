/* eslint-disable no-restricted-globals */
/* eslint-disable jsx-a11y/anchor-is-valid */
import '../styles/signup.css';
import firebaseDb from "../firebase";
import { useState } from 'react';
import {Link, useHistory} from 'react-router-dom';


const Signup = () =>{
    
    const [gender,setgender] = useState("male")
    const history = useHistory();

    function handleWeekdayChange(event:any) {
        setgender(event.target.value)
    }
    const passwordeyechange = () =>{
        const togglePassword = (document.querySelector('#togglePassword') as HTMLInputElement);;
        const password = (document.querySelector('#password') as HTMLInputElement);;
        togglePassword.addEventListener('click', function (e) {
        const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
        password.setAttribute('type', type);
        this.classList.toggle('fa-eye-slash');
        });
    }
    const confirmpasswordeyechange = () =>{
        const togglePassword = (document.querySelector('#toggleConfirmPassword')as HTMLInputElement);
        const password = (document.querySelector('#Confirmpassword')as HTMLInputElement);
        togglePassword.addEventListener('click', function (e) {
        const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
        password.setAttribute('type', type);
        this.classList.toggle('fa-eye-slash');
        });
    }
    const signupValidation = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>) =>{
        let email = (document.getElementById('email') as HTMLInputElement).value;
        let userpassword = (document.getElementById('password') as HTMLInputElement).value;
        let confirmPassword = (document.getElementById('Confirmpassword') as HTMLInputElement).value;
        var mailformat =/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (!email){
            alert("please enter email address");
            return false;
        }
        if(!(email.match(mailformat))){
            alert("please enter valid email address");
            return false;
        }            
        if(userpassword.length < 8){
            alert("Password must be 8 characters long")
            return;
        }   
        if(confirmPassword !== userpassword){
            alert("Password and Congirm Password must be same")
            return;
        }
        registeruser(e);
    }
    const registeruser = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>) =>{
        e.stopPropagation();
        var email = (document.getElementById('email') as HTMLInputElement).value;
        firebaseDb.child("users").orderByChild("email").equalTo(email).once("value",snapshot => {
            if (snapshot.exists()){
              alert("Email address already exist")
              return    
            }
            else{
                firebaseDb.child('users').push({
                    name:(document.getElementById('name') as HTMLInputElement).value,
                    email: (document.getElementById('email') as HTMLInputElement).value,
                    password: (document.getElementById('password') as HTMLInputElement).value,
                    gender: gender,
                    task:[""]
                });
            history.push("/")
            }    
        });
    }
    return(
            <div>
            <div className="text">
            {/* {alert("in signup")} */}
                <p><h1 className="h1signup">Sign Up </h1><h3 className="h3signup">To bring the change in your life</h3></p>
            </div>
            <div className="outercontainersingup"> 
                <form className="formsignup" name="form1" id="formsignup">
                    <h2> Register yourself</h2>
                    <div className="containersignup">
                        <label ><b>Name</b></label>
                        <input type="signup" placeholder="Enter Name" name="uname" id="name" required/>

                        <label ><b>Email address</b></label>
                        <input type="signup" placeholder="ex:someone@abc.com" name="email_add" id="email" required />
                        
                        <label ><b>Password</b></label>
                        <input type="password" placeholder="Enter Password" name="password" id="password" required/>
                        <i className="far fa-eye eyesignuppassword" onClick={passwordeyechange} id="togglePassword"></i>

                        <label ><b>Confirm Password</b></label>
                        <input type="password" placeholder="Confirm Password" name="Confirm password" id="Confirmpassword" required/>
                        <i className="far fa-eye eyesignupconfirmpassword" onClick={confirmpasswordeyechange} id="toggleConfirmPassword"></i>

                        <p onChange={(event) => handleWeekdayChange(event)}>Please select your gender:</p>
                        <input type="radio" id="male" name="gender" value="male" />
                        <label >Male</label>
                        <input type="radio" id="female" name="gender" value="female" />
                        <label >Female</label>
                        <input type="radio" id="other" name="gender" value="other" />
                        <label >Other</label>
                    </div>
                    <div>
                    <p className="conditiontext"> By clicking Create Account, you agree to our Terms, Data Policy and Cookie Policy of IN TIME TEC.</p>
                    <button className="buttonsignup" type="button" onClick={(e) => signupValidation(e)}>Create Account</button>
                    <div style={{marginTop:'30px'}}>Already a member <Link to="/"> login</Link> here</div>
                    </div>
                </form>
            </div>
            </div>
        
        );
    }

 export default Signup;