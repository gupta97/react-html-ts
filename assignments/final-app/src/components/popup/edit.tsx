import React from 'react';
import PropTypes from 'prop-types';

interface MyProps {
  // onClose(): void;
  show: boolean;
};
// type MyState = { show:boolean};

class Modal extends React.Component<MyProps>{
  static propTypes: {
    // onClose: PropTypes.func.isRequired,
    show: PropTypes.Requireable<boolean>; children: PropTypes.Requireable<PropTypes.ReactNodeLike>;
  };

  render() {
    // Render nothing if the "show" prop is false
    if(!this.props.show) {
      return null;
    }

    return (
      <div >
        <div>
          {this.props.children}
        </div>
      </div>
    );
  }
}
Modal.propTypes = {
  // onClose: PropTypes.func.isRequired,
  show: PropTypes.bool,
  children: PropTypes.node
};


export default Modal;