import {Component} from "react";
import PropTypes from 'prop-types'; 
// import "../css/delete.css";


interface MyProps {
  seen: boolean;
};


class Delete extends Component<MyProps>{
  static propTypes: {
    seen: PropTypes.Requireable<boolean>;
  };

  render() {
    if(!this.props.seen) {
      return null;
    }
    return (
      <div >
        <div>
          {this.props.children}
        </div>
      </div>
    );
  }
}
Delete.propTypes = {
  seen: PropTypes.bool,
};


export default Delete;
