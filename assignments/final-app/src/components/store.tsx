import ReactDOM from 'react-dom';
import {store} from './redux/todoComponent';
import {Todo} from './redux/todoComponent';

 
//reducer

const render = () => {
  ReactDOM.render(
    <Todo 
      state={store.getState()}
    />,
    document.getElementById('root')
  )}

render(); // Execute once to render with the initial state.
store.subscribe(render); // Re-render in response to state changes.


