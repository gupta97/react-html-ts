import React, { useState } from "react";
import "../styles/form.css";
import Image from "./Form_image.png"; 	


const Main_Form = () => {
    const [values, setValues] = useState({
        Name: '',
        email: '',
        message: '',
    });
    const [submitted, setSubmitted] = useState(false);
    const [valid, setValid] = useState(false);
    const handleSubmit = (event: { preventDefault: () => void; }) => {
        event.preventDefault();
        if (values.Name &&  values.email && values.message) {
            setValid(true);
        }
        setSubmitted(true);
        if(submitted){
            window.location.reload();
        }
    };

    const handleNameChange = (event: { persist: () => void; target: { value: any; }; }) => {
        event.persist();
        setValues((values) => ({ ...values, Name: event.target.value, }));
    };
    const handleEmailChange = (event: { persist: () => void; target: { value: any; }; }) => {
        event.persist();
        setValues((values) => ({ ...values, email: event.target.value, }));
    };
    const handlemessageChange = (event: { persist: () => void; target: { value: any; }; }) => {
        event.persist();
        setValues((values) => ({ ...values, message: event.target.value, }));
    };
    return (
        <div className="formdiv">
            <form className='register-form ' onSubmit={handleSubmit}>
                <img src={Image} className="formimage" alt=""/>
                <br></br><br></br>
                <label className="formfield">Name:</label>
                <input id="first-name" className="forminput" type="text" placeholder="Name" name="firstName"
                    value={values.Name} onChange={handleNameChange} /><br></br>
                {submitted && !values.Name && <span className="errormsg" id='first-name-error'>Please enter a Name</span>}
                <br></br><br></br>

                <label className="formfield emailfield">Email:</label>
                <input id="email" className="forminput" type="text" placeholder="Email" name="email"
                    value={values.email} onChange={handleEmailChange} /><br></br>
                {submitted && !values.email && <span className="errormsg" id='email-name-error'>Please enter an email</span>}
                <br></br><br></br>

                <label className="formfield messagefield"> Message:</label>
                <textarea cols={29} rows={3} value={values.message} onChange={handlemessageChange}></textarea>
                {submitted && !values.message && <span className="errormsg">Please enter message</span>}
                <br></br><br></br>

                <button type="submit" className="submitbtn" onClick={handleSubmit}>Submit</button>
                <br></br><br></br>
                {valid && <div className='success-message'>Success! Thank you for your response</div>}
            </form>
        </div>
    );
}
export default Main_Form;