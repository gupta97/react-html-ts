import {deleteTodo, addTodo} from './actions'
import firedb from '../../firebase';
import '../../styles/todo.css'
import { useEffect, useState } from 'react';
import { createStore } from 'redux';
import {todoReducer} from './reducer';
import Modal from '../popup/edit';
import CheckIcon from '@material-ui/icons/Check';
import Icon from '@material-ui/icons/DeleteSharp';
import Delete from '../popup/delete';
import {Link} from 'react-router-dom';  

const userEmail: string = sessionStorage.getItem("email")!
let itemIndex: number;
let updatetext: string;

export const store = createStore(todoReducer,(window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__());

export function Todo (props: {state: any; }) {   
  const [newState,setNewState] = useState<any>();
  const [editPopup, setEdit] = useState(false)
  const [deletePopup, setDelete] = useState(false)


  const handleAddTask = () => {
    var item  = (document.getElementById("itemtoadd") as HTMLInputElement).value;
    if (!item){
        return (alert("please enter a task"))
    }
    let tasks;
    (document.getElementById("itemtoadd")as HTMLInputElement).value = "";
    firedb.child('users').orderByChild('email').equalTo(userEmail).once("value",snapshot => {
      let objectKey = Object.keys(snapshot.val())[0]
      let objectArray = snapshot.val()[objectKey]
      tasks = objectArray.task
      tasks = [...tasks, item]
      firedb.child('users').child(objectKey).update({
        task: tasks
          });
    });
    store.dispatch(addTodo(item))
    setNewState(store.getState())
  }
  const handleDeleteTask = () =>{

    let tasks
    firedb.child('users').orderByChild('email').equalTo(userEmail).once("value",snapshot => {
      let objectKey = Object.keys(snapshot.val())[0]
      let objectArray = snapshot.val()[objectKey]
      tasks = objectArray.task
      tasks = [...tasks.slice(0,itemIndex),...tasks.slice(itemIndex+1)]
      firedb.child('users').child(objectKey).update({
        task: tasks
          });
    });
    store.dispatch(deleteTodo(itemIndex))
    console.log(store.getState())
    setNewState(store.getState())
    setDelete(false)
    setEdit(false)
  }
  const checked = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>,index: number) =>{
      e.stopPropagation();
      let list = document.querySelector('ul');
      if(list){
          let li = list.getElementsByTagName("li");
          let currentli = li[index];
          if (currentli.className === "checked"){
              currentli.className = "demo";
          }
          else{
              currentli.className = "checked"
          }
      }
  }   
  useEffect(() => {
    let load = true;
    if (load){
      firedb.child('users').orderByChild('email').equalTo(userEmail).once("value",snapshot => {
        let objectKey = Object.keys(snapshot.val())[0]
        let objectArray = snapshot.val()[objectKey]
        let tasks = objectArray.task
        for (var i = 0; i< (tasks.length);i++){
            store.dispatch(addTodo(tasks[i]))
        }
        setNewState(Object(store.getState()))
    });
    }
    return () => { load = true; }
    },[]); 

  const editTask = (index: number, data: string) =>{
    setEdit(!editPopup)
    let date  = Date()
    console.log(date)
    itemIndex = index;
    updatetext = data;
  }
  const deleteTask = (index: number) =>{
    itemIndex = index;
    setDelete(!deletePopup)
    setEdit(false)
  }
  const saveTask = () =>{
    var item  = (document.getElementById("update") as HTMLInputElement).value;
    if (!item){
        return (alert("please enter something"))
    }
    let tasks;
    firedb.child('users').orderByChild('email').equalTo(userEmail).once("value",snapshot => {
      let objectKey = Object.keys(snapshot.val())[0]
      let objectArray = snapshot.val()[objectKey]
      tasks = objectArray.task
      console.log("before", tasks)
      tasks = [...tasks.slice(0,itemIndex),...tasks.slice(itemIndex+1)]
      tasks = [...tasks, item]
      console.log("after", tasks)
      firedb.child('users').child(objectKey).update({
        task: tasks
          });
    });
    store.dispatch(addTodo(item))
    store.dispatch(deleteTodo(itemIndex))
    setEdit(false)
    setNewState(store.getState())
}
const discardTask = () =>{
  setEdit(false)
}
const cancelDelete = () =>{
  setDelete(false) 
  setEdit(false)

}

  return (  
    <div className='back'>
      <button className="profilebutton" ><Link to="/profile">Profile</Link></button>
      <button className="logoutbutton">Logout</button>
      <Modal show={editPopup}>
      <form>
        <div className="update">
          <textarea className="editinput" id="update" defaultValue={[updatetext]}/>
        </div>
      </form>
      <CheckIcon className="buttonsave" onClick={saveTask}></CheckIcon>
      <Icon className="buttondiscard" onClick={discardTask}></Icon>
      </Modal>
      <Delete seen={deletePopup}>
        <div className="confirmdeletebox">
              <p className="confirmdeletetext"><br></br>Are you sure you want to delete this task?</p>
        </div>
      <button className="buttondelete" onClick={handleDeleteTask}>Confirm</button>
      <button className="buttoncancel" onClick={cancelDelete}>Cancel</button>
      </Delete>
      <p className="heading">ToDo List</p>
      <div className="aligndiv">  
              <input className="inputapp" type="text" id="itemtoadd" placeholder="Add a Task"/>
              <button className="buttonadd" onClick={handleAddTask}>Add Task</button>
      </div>    
      <div>
        <ul id="myUL">
            {newState
            ? newState.map((data: string, index: number) => 
              <li onClick={() => {editTask(index,data)}}>{data}
              <span onClick={(e) => {checked(e,index)}}><input type="checkbox" className="markdone"/></span>
              <span className="close" onClick={(e) => {deleteTask(index)}}>&times;</span>
              </li>)
            :<p>Add task to start scheduling your time  </p>
            }
        </ul>
      </div>
    </div>
  )
  }
   