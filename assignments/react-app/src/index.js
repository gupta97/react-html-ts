import ReactDOM from 'react-dom';
import './index.css'
import Todo from './store'

ReactDOM.render(
    <Todo />,
    document.getElementById('root')
)