import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import './index.css'

 

const initialState: any= [];
export const addTodo = (item: string) => {
  return {type: 'addTodo', payload: item} 
}

export const deleteTodo = (index: number) => {
    return {type: 'deleteTodo', payload: index} 
}

export const todoReducer = (state = initialState, action: { type: any; payload: any; }) =>{
    switch (action.type) {
        case "addTodo":
        return [...state, action.payload]
        
        case 'deleteTodo':
            return[...state.slice(0,action.payload),...state.slice(action.payload+1)]
        default:
            return state
    }
}
const store = createStore(todoReducer);

const render = () => {
  ReactDOM.render(
    <Todo 
      state={store.getState()}
    />,
    document.getElementById('root')
  )
}
 
render(); // Execute once to render with the initial state.
store.subscribe(render); // Re-render in response to state changes.

function Todo(props: { state: any; }) {
    const state = props.state;

    const handleAddTask = () => {
        var item  = (document.getElementById("itemtoadd") as HTMLInputElement).value;
        (document.getElementById("itemtoadd")as HTMLInputElement).value = "";
        store.dispatch(addTodo(item));
    }
    const handleDeleteTask = (index: number) =>{
        if (state){
            console.log("yes state")
        }
        else{
            console.log("no state")
        }
        store.dispatch(deleteTodo(index))
    }
    const checked = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>,index: number) =>{
        e.stopPropagation();
        let list = document.querySelector('ul');
        if(list){
            let li = list.getElementsByTagName("li");
            let currentli = li[index];
            if (currentli.className === "checked"){
                currentli.className = "demo";
            }
            else{
                currentli.className = "checked"
            }
        }
    }   
  return (  
    <div className='back'>
      <p>ToDo List</p>
      <div className="aligndiv">  
              <input className="inputapp" type="text" id="itemtoadd" placeholder="Add a Task"/>
              <button className="buttonadd" onClick={handleAddTask}>Add Task</button>
      </div>    
      <div>
        <ul id="myUL">
            {state
            ?state.map((data: string, index: number) => 
                <li >{data}
                <span onClick={(e) => {checked(e,index)}}><input type="checkbox" className="markdone"/></span>
                <span className="close" onClick={(e) => {handleDeleteTask(index)}}>&times;</span>
                </li>)
            :<p>Add something to start scheduling your day</p>
            }
        </ul>
      </div>
    </div>
  )
}
 
export default Todo;