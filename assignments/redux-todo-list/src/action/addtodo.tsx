export const addTodo = (item: string) => {
    return {type: 'addTodo', payload: item} 
  }
  
  export const deleteTodo = (index: any) => {
      return {type: 'deleteTodo', payload: index} 
  }