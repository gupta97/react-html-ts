/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from 'react';
import ContactForm from "./ContactForm";
import firebaseDb from "./firebase";

const Contacts = () => {
    var blank: any = [];
	var [currentId, setCurrentId] = useState('');
    var [contactObjects, setContactObjects] = useState(blank)

    //Once components load complete
    useEffect(() => {
        firebaseDb.child('contacts').on('value', DataSnapshot => {
            if (DataSnapshot.val() != null) {
                setContactObjects({
                    ...DataSnapshot.val()
                });
            }
        })
    }, [])
    const addOrEdit = (obj: any) => {
        if (currentId === '')
            firebaseDb.child('contacts').push(obj)
        else
            firebaseDb.child(`contacts/${currentId}`).set(obj)
    }
    const onDelete = (id: string) => {
        if (window.confirm("Are you sure to delete this contact?")) {
            firebaseDb.child(`contacts/${id}`).remove()
        }
    }

  return (
    <div className="row">
        <div className="col-md-8 offset-md-2">
                    <h1 className="display-4 text-center">Employee Details</h1>
            <div className="row">
                <div className="col-md-5">
                    <ContactForm {...({ currentId, contactObjects, addOrEdit })} ></ContactForm>
                </div>
                <div className="col-md-7">
                    <table className="table table-borderless table-stripped">
                        <thead className="thead-light">
                            <tr>
                                <th>Name</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                Object.keys(contactObjects).map((key) => (
                                    <tr key={key}>
                                        <td>{contactObjects[key].fullName}</td>
                                        <td>{contactObjects[key].username}</td>
                                        <td>{contactObjects[key].email}</td>
                                        <td className="bg-light">
                                            <a className="btn text-primary" onClick={() => { setCurrentId(key) }}>
                                                <i className="fas fa-pencil-alt"></i>
                                            </a>
                                            <a className="btn text-danger" onClick={() => { onDelete(key) }}>
                                                <i className="far fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    );
}

export default Contacts;