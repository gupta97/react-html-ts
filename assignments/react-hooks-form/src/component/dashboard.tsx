import "../css/dashboard.css"
import Form from "./form"
import Todoredux from './store'
import Contact from './Contacts'


import {BrowserRouter as Router,Switch,Route,Link} from "react-router-dom";



const Homepage = () =>{
        return(
            <Router>
                <div>
                <div className="headerspantext">
                </div>
                    <span className="headerbuttons ">
                        <Link to="/" className="buttontext">Post Query</Link>
                        <Link to="redux" className="buttontext">To-Do</Link>
                        <Link to="contactapi" className="buttontext">API</Link>
                    </span>
                    <Switch>
                        <Route exact path="/">
                            <Form/>
                        </Route>
                        <Route path="/redux">
                            <Todoredux state={[]}/>,
                        </Route>
                        <Route path="/contactapi">
                            <Contact />
                        </Route>
                    </Switch>
                </div>
                
        </Router>
        )
}
export default Homepage;
