import ReactDOM from 'react-dom';
import './index.css';
import Form from "./component/form";
import Dashboard from "./component/dashboard"

ReactDOM.render(
  <div>
  <Dashboard>
    <Form/>
  </Dashboard>
</div>,
document.getElementById("root")
);