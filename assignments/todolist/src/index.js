import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {Todo, store} from './store/todo';
// import {store} from './store/todo'


const render = () => {
  ReactDOM.render(
    <Todo state={store.getState}/>,
    document.getElementById('root')
  )
}
 
render(); // Execute once to render with the initial state.
store.subscribe(render); // Re-render in response to state changes.

