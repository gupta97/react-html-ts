import { createStore } from 'redux';
import {todoReducer} from '../reducer/todo';
import {addTodo, deleteTodo} from '../action/todo';



export const store = createStore(todoReducer);


export function Todo(props: { state: any; }) {
    const state = props.state;
    console.log(state)

    const handleAddTask = () => {
        const item  = (document.getElementById("itemtoadd") as HTMLInputElement).value;
        (document.getElementById("itemtoadd")as HTMLInputElement).value = "";
        store.dispatch(addTodo(item));
    }
    const handleDeleteTask = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>,index: number) =>{
        e.stopPropagation();
        store.dispatch(deleteTodo(index))
    }
    const checked = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>,index: number) =>{
        e.stopPropagation();
        let list = document.querySelector('ul');
        if(list){
            let li = list.getElementsByTagName("li");
            let currentli = li[index];
            if (currentli.className === "checked"){
                currentli.className = "demo";
            }
            else{
                currentli.className = "checked"
            }
        }
    }   
  return (  
    <div className='back'>
        <p className='heading'>ToDo List</p>
        <div className="aligndiv">  
                <input className="inputapp" type="text" id="itemtoadd" placeholder="Add a Task"/>
                <button className="buttonadd" onClick={handleAddTask}>Add Task</button>
        </div>    
        <div>
        <ul id="myUL">
            {state
            ?state.map((data: string, index: number) => 
                <li>{data}
                <span onClick={(e) => {checked(e,index)}}><input type="checkbox" className="markdone"/></span>
                <span className="close" onClick={(e) => {handleDeleteTask(e,index)}}>&times;</span>
                </li>)
            : <p>Add something to start scheduling your day</p>
            }
        </ul>
        </div>
    </div>
  )
}
