import ReactDom from 'react-dom';
import Todo from './components/todolist'

ReactDom.render(
    <div>
        <Todo />
    </div>,
    document.getElementById('root')
    );