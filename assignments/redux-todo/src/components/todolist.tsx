import React, { Component } from "react";
import "../styles/ToDoApp.css";
import Modal from "./edit";
import CheckIcon from '@material-ui/icons/Check';
import Icon from '@material-ui/icons/DeleteSharp';

let localdata:any = JSON.parse(localStorage.getItem("user1")!)
let updatetext: any;
let item: number;

type MyProps = {};
type MyState = { isOpen: boolean, isvisible:boolean, index: number};


class tsxlist extends Component<MyProps, MyState> {
    constructor(props: MyProps | Readonly<MyProps>) {
        super(props);
        this.state = {isOpen:false, isvisible:false, index:0};
      }
      
// const ToDo = () =>{
    AddItem = () => {
        var item =(document.getElementById('itemtoadd') as HTMLInputElement).value;
        localdata = JSON.parse(localStorage.getItem("user1")!)
        if (item === ''){
            alert("You must write something!");
        }
        else if (localdata){
            localdata.push(item)
            localStorage.setItem("user1",JSON.stringify(localdata))
        }
        else{
            var newdata = []
            newdata.push(item)
            localStorage.setItem("user1",JSON.stringify(newdata))
        }
        (document.getElementById("itemtoadd") as HTMLInputElement).value = "";
        localdata = JSON.parse(localStorage.getItem("user1")!)
        if (localdata){
            return (JSON.parse(localStorage.getItem("user1")!))
        }
    }
    checked = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>,index: number) =>{
        e.stopPropagation();
        let list = document.querySelector('ul');
        if(list){
            let li = list.getElementsByTagName("li");
            let currentli = li[index];
            if (currentli.className === "checked"){
                currentli.className = "demo";
            }
            else{
                currentli.className = "checked"
            }
        }
    }
    confirm = () =>{
        let list = document.querySelector('ul');    let currentli
        if (list){
            let li = list.getElementsByTagName("li")
            currentli = li[item]
            var localData = JSON.parse(localStorage.getItem("user1")!);
            localData.splice(item, 1)
            localStorage.removeItem("user1")
            localStorage.setItem("user1", JSON.stringify(localData))
            currentli.className = "hide";
        }
        this.setState({
            isvisible: !this.state.isvisible
        });
    
    }
      cancel = () =>{
        this.setState({
            isvisible: !this.state.isvisible
        });
      }
    
    toggleModal = () => {
        this.setState({
          isOpen: !this.state.isOpen
        });
      }
    close = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>, index: number) =>{
        e.stopPropagation();
        item = index;
        this.setState({
            isvisible: !this.state.isvisible,
            index:index
        })
    }
    edit = (index: number, data: number) =>{
        this.toggleModal()
        updatetext = JSON.parse(localStorage.getItem("user1")!)[index]
        localStorage.setItem("index", JSON.stringify(index))
    }
    save = () =>{
        var data: any = (document.getElementById("update") as HTMLInputElement).value;
        var localarray: any = JSON.parse(localStorage.getItem("user1")!)
        var oldtext: number = JSON.parse(localStorage.getItem("index")!)
        localarray[oldtext] = data
        localStorage.setItem("user1", JSON.stringify(localarray))
        localStorage.removeItem("index")
        this.setState({
            isOpen: !this.state.isOpen
          });
    }
    discard = () =>{
        this.setState({
            isOpen: !this.state.isOpen
          });
    }
    render(){
        return(
            <div className="back">
                {this.state.isOpen && <Modal show={this.state.isOpen}>
                    <form>
                        <div className="confirmdelete">
                            <textarea className="editinput" id="update" defaultValue={[updatetext]}/>
                        </div>
                    </form>
                    <CheckIcon className="buttonsave" onClick={this.save}></CheckIcon>
                    <Icon className="buttondiscard" onClick={this.discard}></Icon>
                </Modal>}
                <p className="heading">To-Do List</p>
                <div className="aligndiv"> 
                    <form>
                        <input className="inputapp" type="text" id="itemtoadd" placeholder="Add a Task"/>
                        <button className="buttonadd" onClick={this.AddItem}>Add Task</button>
                    </form>
                </div>    
                <div>
                    {/* {this.state.isvisible && <Delete seen={this.state.isvisible}>
                    <div className="container">
                        <div className="innercontainer">
                            <span>
                                <p className="confirmtext">Are you sure you want to delete it permanently?</p>
                                <button className="confirm" onClick={this.confirm} >Delete</button>
                                <button className="cancel" onClick={this.cancel}>Cancel</button>
                            </span>
                        </div>
                    </div>
                    </Delete>}   */}
                    {/* <Delete seen={this.state.isvisible} index={item}/> */}
                    <ul id="myUL">
                        {localdata.map((data: number, index: number) => 
                            <li onClick={() => {this.edit(index,data)}}>{data}
                            <span onClick={(e) => {this.checked(e,index)}}><input type="checkbox" className="markdone"/></span>
                            <span className="close" onClick={(e) => {this.close(e,index)}}>&times;</span>
                            </li>)
                        }
                    </ul>
                </div>
            </div>
        );
    }
}
export default tsxlist;


